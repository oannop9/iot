<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>relationships</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@200;400&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Prompt', sans-serif;

        }

        nav {
            font-size: 18px;
        }

        p {
            font-size: 16px;
        }
    </style>

</head>

<body style="background-color: #D8E3E7;">
    <div class="container">
        <?php include('includes/header.php');?>
        <div class="row">
            <div class="col">
                <div class="card border-secondary">
                    <div class="card-body text-secondary">
                        <h1 class="text-center">Big Data คือ</h1><br>
                        &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;ข้อมูลขนาดมหาศาลที่เกิดขึ้น
                        ไม่มีโครงสร้างชัดเจน หนึ่งในตัวอย่างที่เราจะเห็นได้ง่ายคือข้อมูลจากยุคโซเชียล
                        ผู้ใช้เป็นคนสร้างขึ้นมา ซึ่งนอกจากเนื้อหาในโลกออนไลน์แล้ว ยังมีข้อมูลอีกประเภทหนึ่งคือ
                        ข้อมูลจากอุปกรณ์ที่เราใช้หรือสวมใส่ เช่น สายรัดวัดชีพจรตอนออกกำลังกาย ตัวอย่างเช่น
                        แบรนด์ไนกี้ตั้งโจทย์ขึ้นมาว่า เราจะรู้ได้อย่างไรว่าลูกค้าซื้อรองเท้าแล้วนำไปใส่วิ่งจริง
                        แต่ตอนนี้พิสูจน์ได้แล้วเพราะว่าไนกี้ใช้ IoT กับสินค้าของเขา
                        ยิ่งไปกว่านั้นตอนนี้ลูกค้าไม่ได้ผลิตข้อมูลที่นำไปสู่ Big Data จากการโพสต์ คอมเมนต์ กดไลค์
                        หรือแชร์เพียงอย่างเดียว แต่เกิดจากกิจกรรมในไลฟ์สไตล์ที่ธุรกิจหรือแบรนด์นำไปจับคู่กับสินค้า
                        แล้วสร้างเป็นเนื้อหาที่โดนใจผู้รับสารขึ้นมา สิ่งสำคัญคือ
                        ข้อมูลพวกนี้บอกว่ามีอะไรเกิดขึ้นได้อย่างเดียว สิ่งสำคัญกว่าคือ ธุรกิจ องค์กร และแบรนด์ต่างๆ
                        จะเปลี่ยนข้อมูลพวกนี้เป็นประโยชน์ได้อย่างไร ทำอย่างไรข้อมูลจึงจะสามารถบอกได้ว่า
                        ‘ทำไมสิ่งต่างๆเหล่านั้นถึงเกิดขึ้น’ จุดนี้จึงทำให้เรารู้จักความต้องการที่แท้จริงของผู้บริโภค
                        (Consumer Insight) และรู้ว่าต้องทำอย่างไรให้ธุรกิจหรือบริการของเราจะมีประสิทธิภาพยิ่งขึ้น<br>
                        https://www.aware.co.th/iot/
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 30px;">
            <div class="col">
                <img src="https://awareth.aware-cdn.net/wp-content/uploads/2018/03/IoT-02.jpg" width="100%" alt="">
            </div>
            <div class="col">
                <div class="card border-secondary">
                    <div class="card-body text-secondary">
                        <h1>Internet of Things และ Big Data สัมพันธ์กันอย่างไร</h1><br>
                        &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;ปัจจุบัน Internet of Things
                        สามารถตอบสนองความต้องการทางด้านการใช้งานของเราได้มากขึ้น
                        สาเหตุเพราะอุปกรณ์อิเล็กทรอนิกส์ต่างๆมีราคาถูกลง ทำให้เกิดการใช้งานจริงมากขึ้น มีการค้นพบ Use
                        Case ใหม่ๆในธุรกิจ ทำให้ผู้ผลิตได้เรียนรู้และคอยแก้โจทย์ เพื่อเพิ่มประสิทธิภาพให้ตรงใจผู้ใช้
                        ก่อให้เกิดนวัตกรรมใหม่ๆ ยิ่งไปกว่านั้น Internet of Things มีการเชื่อมโยงกันอย่างเป็นระบบ
                        เราจึงเริ่มเห็นธุรกิจที่หันมาให้ความสนใจ Internet of Things
                        ในแง่ที่มันสามารถช่วยแก้ปัญหาทางธุรกิจ ทางสังคม และช่วยแก้ไขปัญหาชีวิตประจำวันได้
                        โดยการนำเอาข้อมูลหรือ Big Data เข้ามาใช้ในการพัฒนาเพื่อตอบสนองความต้องการของแต่ละรูปแบบ
                        ถึงตรงนี้แล้วคงจะสงสัยใช่มั้ยคะว่า Big data เกี่ยวข้องอย่างไรกับ Internet of Things<br>
                        https://www.aware.co.th/iot/
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('includes/footer.php') ?>
    </div>
</body>

</html>