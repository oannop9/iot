<?php
session_start();
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark rounded border border-light">
    <!-- Image and text -->
    <div class="row">
        <div class="col-sm-3">
            <a class="navbar-brand" href="index.php">
                IOT (Internet Of Things)
            </a>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-2"></div>
        <div class="col-sm-2"></div>
        <div class="col-sm-2"></div>
        <div class="col-sm-1" style="font-size: 14px;">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="concept.php">concept</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="relationships.php">relationships</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="benefits.php">benefits</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link bg-danger" href="scripts/logout.php">logout</a>
                    </li>
                </ul>

            </div>
        </div>

    </div>
</nav>