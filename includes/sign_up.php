<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Sign Up</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="scripts/signup_db.php" method="post">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3" for="user">Username :</label>
                        <div class="col-sm-9">
                        <input class=" border border-secondary form-control" type="text" name="input_username" id="input_username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3" for="user">Password :</label>
                        <div class="col-sm-9">
                        <input class=" border border-secondary form-control" type="text" name="input_password" id="input_password">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>