<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@200;400&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Prompt', sans-serif;

        }

        nav {
            font-size: 18px;
        }

        p {
            font-size: 16px;
        }
    </style>

</head>

<body style="background-color: #EDFFEC;">
    <div class="container">
        <?php include('includes/header.php') ?>
        <div class="row" style="padding-top: 10%;">
            <div class="col-lg-2"></div>
            <div class="col-lg-3">
                <img src="images/profile.jpg" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-lg-5">
                <div class="card mb-3" style="max-width: 100%;">
                    <div class="card-body">
                        <h5 class="card-title">Hello</h5>
                        <p class="card-text">
                            My name's Oannop boondee <br>
                            My nickname's New <br>
                            I'm studying at Southeast Bangkok <br>
                            My mojor was science and technology <br>
                            My student id is : '6311413200003'
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-dark">
            

        </footer>
</body>

</html>