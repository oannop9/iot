<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@200;400&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Prompt', sans-serif;

        }

        nav {
            font-size: 18px;
        }

        p {
            font-size: 16px;
        }
    </style>

</head>

<body style="background-color: #D8E3E7;">
    <div class="container">
        <?php include('includes/header.php');?>
        <div class="row" style="padding-top: 5px;">
            <div class="col">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100"
                                src="https://www.zephyrnetworks.com/wp-content/uploads/2021/01/image_1.jpg"
                                alt="First slide">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row" style="padding-top: 30px;">
            <div class="col">
                <img src="https://www.ops.go.th/main/images/2561/ICTC/Untitled-1.png" width="100%" alt=""></div>
            <div class="col">
                <div class="card border-secondary">
                    <div class="card-body text-secondary">
                        <h1>IOT คือ?</h1><br>
                        &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;IoT หรือ Internet of Things
                        (อินเทอร์เน็ตของสรรพสิ่ง) หมายถึง วัตถุ อุปกรณ์ พาหนะ สิ่งของเครื่องใช้
                        และสิ่งอำนวยความสะดวกในชีวิตอื่น ๆ ที่มนุษย์สร้างขึ้นโดยมีการฝังตัวของวงจรอิเล็กทรอนิกส์
                        ซอฟต์แวร์ เซ็นเซอร์ และการเชื่อมต่อกับเครือข่าย ซึ่งวัตถุสิ่งของเหล่านี้
                        สามารถเก็บบันทึกและแลกเปลี่ยนข้อมูลกันได้ อีกทั้ง
                        สามารถรับรู้สภาพแวดล้อมและถูกควบคุมได้จากระยะไกล
                        ผ่านโครงสร้างพื้นฐานการเชื่อมต่อเข้ากับสมาร์ทโฟนเท่านั้น แต่ IoT
                        สามารถประยุกต์ใช้กับอุปกรณ์ทุกอย่างที่ถูกออกแบบมาให้เชื่อมโยงกันได้บนเครือข่ายอินเทอร์เน็ตเพื่อที่จะสามารถสื่อสารกันได้ <br>
                        ขอบคุณข้อมูลดีๆ จาก https://www.aware.co.th/iot
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 40px;">

            <div class="col">
                <img src="https://siambc.com/wp-content/uploads/2019/08/IoT-%E0%B8%84%E0%B8%B7%E0%B8%AD.jpg"
                    width="100%" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body text-secondary">
                        <h1>ทำไมต้องมี IoT</h1><br>
                        &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;ต้องยอมรับว่า IoT
                        ถือเป็นเทคโนโลยีที่สำคัญและมีประโยชน์ต่อแวดวงอุตสาหกรรมหลายอย่าง และเมื่อพูดถึงประโยชน์ของ IoT
                        สำหรับคนทั่วไป ก็คงส่งผลดีต่อการใช้ชีวิตประจำวันภายในบ้าน หากเน้นประโยชน์ของ IoT
                        ที่มีต่อที่อยู่อาศัย ก็ต้องบอกว่าบ้านหรือคอนโดที่มีระบบ Internet of Things
                        จะมีลักษณะเป็นสมาร์ทโฮม กล่าวคือเป็นบ้านที่เน้นการเชื่อมต่อกับอินเตอร์เน็ตเป็นหลัก
                        สิ่งของทุกอย่างล้วนรับส่งข้อมูลตามคำสั่งที่ป้อนเข้าไป

                        นอกเหนือจากภาพความทันสมัยและสะดวกสบายแล้ว การใช้เทคโนโลยี IoT
                        ยังมีประโยชน์อีกหลายอย่างที่เราควรพิจารณาไว้ เพื่อนำมาใช้ในที่อยู่อาศัยต่อไป
                        และนี่ก็เป็นเหตุผลว่าทำไมเราควรติดตั้งเทคโนโลยีดังกล่าวไว้ในบ้าน<br>
                        ขอบคุณข้อมูลดีๆ จาก https://www.aware.co.th/iot
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 10px;">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        1. ควบคุมการทำงานอุปกรณ์ต่าง ๆ ง่ายขึ้น
                    </div>
                    <div class="card-body text-secondary">
                        <blockquote class="blockquote mb-0">
                            <img src="https://aisdc.ais.co.th/blog/images/trend_iots/image_1.jpg" width="100%" alt="">
                            <br><br>
                            <p>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;เทคโนโลยี IoT
                                จะช่วยให้ควบคุมกระบวนการทำงานของอุปกรณ์อิเล็กทรอนิกส์ต่าง ๆ ได้มีประสิทธิภาพยิ่งขึ้น โดย
                                IoT จะเก็บข้อมูลว่ากิจวัตรในแต่ละวันเป็นอย่างไร ต้องทำกิจกรรมหรืองานอะไรบ้าง
                                จากนั้นก็จะประมวลผล เพื่อนำไปเชื่อมต่อและสั่งการทำงานต่อไป
                                เราไม่จำเป็นต้องทำงานเองทุกอย่าง เพราะระบบ IoT ที่ได้รับการป้อนข้อมูลจะดำเนินการให้เอง
                                เช่น ตู้เย็นที่เชื่อมต่อกับระบบ IoT จะแจ้งเตือนเจ้าของบ้านว่ามีโยเกิร์ตใกล้เสียอีกสองวัน
                                ซึ่งช่วยให้เราเตรียมกำจัดทิ้งได้เลย โดยไม่ต้องมานั่งคัดแยกและดูวันหมดอายุของของกินเอง
                                เป็นต้น<br>
                        ขอบคุณข้อมูลดีๆ จาก https://www.aware.co.th/iot</p>
                        </blockquote>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        2. ประหยัดค่าใช้จ่าย
                    </div>
                    <div class="card-body text-secondary">
                        <blockquote class="blockquote mb-0">
                            <img src="https://www.iliketax.com/wp-content/uploads/2020/01/%E0%B8%84%E0%B9%88%E0%B8%B2%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B8%88%E0%B9%88%E0%B8%B2%E0%B8%A2%E0%B8%84%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B8%88%E0%B9%88%E0%B8%B2%E0%B8%A2%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88.png"
                                width="100%" alt="">
                            <br><br>
                            <p>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                &nbsp;สมาร์ทโฮมจะใช้พลังงานในการทำงานแต่ละอย่างได้คุ้มค่าและมีประสิทธิภาพมากกว่า
                                เพราะเราสามารถกำหนดตั้งค่าการใช้งานอุปกรณ์อิเล็กทรอนิกส์ที่เชื่อมต่อกับระบบ IoT
                                ซึ่งเอื้อต่อการประหยัดค่าใช้จ่ายโดยรวมได้ นอกจากนี้
                                เราสามารถตั้งค่าให้อุปกรณ์เหล่านั้นทำงานทุกอย่างได้เอง
                                โดยระบบจะชัตดาวน์อุปกรณ์อื่นที่ไม่ได้ใช้งานขณะนั้น
                                ส่งผลให้การใช้พลังงานที่ไม่จำเป็นลดลงตามไปด้วย กลายเป็นต้นแบบบ้านประหยัดพลังงาน<br>
                        ขอบคุณข้อมูลดีๆ จาก https://www.aware.co.th/iot</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 10px;">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        3. เป็นมิตรต่อสิ่งแวดล้อม
                    </div>
                    <div class="card-body text-secondary">
                        <blockquote class="blockquote mb-0">
                            <img src="https://pbs.twimg.com/media/D0PcfP3VYAEHKt3.png"
                                width="100%" alt="">
                            <br><br>
                            <p>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;นอกจากจะช่วยประหยัดพลังงานและค่าใช้จ่ายแล้วนั้น
                                การประหยัดพลังงานยังส่งผลดีต่อสิ่งแวดล้อม ทำให้สภาพแวดล้อมถูกทำลายน้อยลง
                                อีกทั้งยังส่งเสริมการใช้ชีวิตคุณภาพในสภาพแวดล้อมปลอดมลพิษยิ่งขึ้น<br>
                        ขอบคุณข้อมูลดีๆ จาก https://www.aware.co.th/iot</p>
                        </blockquote>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        4. มีความปลอดภัยสูง
                    </div>
                    <div class="card-body text-secondary">
                        <blockquote class="blockquote mb-0">
                            <img src="https://www.itdigitserve.com/wp-content/uploads/2016/07/network-security.jpg"
                                width="100%" alt="">
                            <br><br>
                            <p>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                &nbsp;ระบบรักษาความปลอดภัยของสมาร์ทโฮมจะช่วยปกป้องความเสียหายและการสูญหายของสินทรัพย์ภายในบ้านได้เป็นอย่างดี
                                เพราะระบบ IoT
                                จะมีชุดคำสั่งที่ช่วยควบคุมและดูแลความปลอดภัยสภาพแวดล้อมทั้งภายในและภายนอกบ้าน
                                ไม่ว่าจะเป็น กล้องวงจรปิด ระบบปลดล็อกสมาร์ทโฟน
                                หรือแม้กระทั่งระบบเซ็นเซอร์ดักจับการเคลื่อนไหวหรือควันอื่น ๆ
                                อุปกรณ์เหล่านี้จะทำงานร่วมกันและส่งสัญญาณแจ้งเตือนในกรณีที่พบสิ่งผิดปกติ นอกจากนี้
                                เรายังตรวจสอบหรือดูสภาพแวดล้อมภายในและรอบบ้านว่าเป็นอย่างไรในขณะที่อยู่ที่อื่นก็ได้<br>
                        ขอบคุณข้อมูลดีๆ จาก https://www.aware.co.th/iot</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
        <?php include('includes/footer.php') ?>
    </div>

</body>

</html>