<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

</head>

<body>
    <div class="container">
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first">
                    <i class="bi bi-app-indicator"></i>
                    <img src="./images/app-indicator.svg" style="width: 20%; padding-top: 25px" id="icon"
                        alt="User Icon" />
                    <h1>IOT LOGIN</h1>
                </div>

                <!-- Login Form -->
                <form action="scripts/login_db.php" method="POST">
                    <input type="text" id="user" class="fadeIn second" name="user" placeholder="username">
                    <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
                    <input type="submit" class="btn btn-primary" value="Log In">
                </form>
                <!-- Remind Passowrd -->
                <div id="formFooter">
                    <button type="button" class="btn btn-secondary sign_up" data-toggle="modal"
                        data-target="#exampleModalCenter">
                        Sign up
                    </button>
                </div>

            </div>
        </div>
    </div>




</body>
<?php
include('includes/sign_up.php');
?>

<script>
    $(ducument).ready(function () {
        $('.sign_up').click(function () {
            $('#exampleModalCenter').modal('show');
        });
    });
</script>

</html>