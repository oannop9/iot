<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>concept</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@200;400&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Prompt', sans-serif;

        }

        nav {
            font-size: 18px;
        }

        p {
            font-size: 16px;
        }
    </style>

</head>

<body style="background-color: #D8E3E7;">
    <div class="container">
        <?php include('includes/header.php');?>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="row " style="padding-top: 30px;">
                    <img  class="rounded" src="https://awareth.aware-cdn.net/wp-content/uploads/2018/03/IoT-Facbook-Kevin-Quote-1-700x525.jpg"
                        width="100%" alt="">
                    <div class="row" style="padding-top: 10px;">
                        <div class="card border-secondary">
                            <div class="card-body text-secondary">
                                <h1>แนวคิด Internet of Things</h1><br>
                                &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;เดิมมาจาก Kevin Ashton
                                บิดาแห่ง
                                Internet of Things ในปี 1999 ในขณะที่ทำงานวิจัยอยู่ที่มหาวิทยาลัย Massachusetts
                                Institute of
                                Technology หรือ MIT เขาได้ถูกเชิญให้ไปบรรยายเรื่องนี้ให้กับบริษัท Procter & Gamble (P&G)
                                เขาได้นำเสนอโครงการที่ชื่อว่า Auto-ID Center ต่อยอดมาจากเทคโนโลยี RFID
                                ที่ในขณะนั้นถือเป็นมาตรฐานโลกสำหรับการจับสัญญาณเซ็นเซอร์ต่างๆ( RFID Sensors)
                                ว่าตัวเซ็นเซอร์เหล่านั้นสามารถทำให้มันพูดคุยเชื่อมต่อกันได้ผ่านระบบ Auto-ID ของเขา
                                โดยการบรรยายให้กับ P&G ในครั้งนั้น Kevin ก็ได้ใช้คำว่า Internet of Things
                                ในสไลด์การบรรยายของเขาเป็นครั้งแรก โดย Kevin
                                นิยามเอาไว้ตอนนั้นว่าอุปกรณ์อิเล็กทรอนิกส์ใดๆก็ตามที่สามารถสื่อสารกันได้ก็ถือเป็น
                                “internet-like”
                                หรือพูดง่ายๆก็คืออุปกรณ์อิเล็กทรอนิกส์ที่สื่อสารแบบเดียวกันกับระบบอินเตอร์เน็ตนั่นเอง
                                โดยคำว่า
                                “Things” ก็คือคำใช้แทนอุปกรณ์อิเล็กทรอนิกส์ต่างๆเหล่านั้น<br>
                                https://www.aware.co.th/iot/
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row" style="padding-top: 30px;">
                    <img src="http://www.befirstnetwork.com/wp-content/uploads/2017/02/Internet_of_things_signed_by_the_author.jpg"
                        width="100%" alt="">
                </div>
                <div class="row" style="padding-top: 30px;">
                    <div class="card border-secondary">
                        <div class="card-body text-secondary">
                            &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;ต่อมาในยุคหลังปี 2000
                            มีอุปกรณ์อิเล็กทรอนิกส์ถูกผลิตออกจัดจำหน่ายเป็นจำนวนมากทั่วโลก จึงเริ่มมีการใช้คำว่า Smart
                            ซึ่งในที่นี้คือ Smart Device, Smart Grid, Smart Home, Smart Network, Smart Intelligent
                            Transportation ต่างๆเหล่านี้ ล้วนถูกฝัง RFID Sensors เสมือนกับการเติม ID และสมอง
                            ทำให้มันสามารถเชื่อมต่อกับโลกอินเตอร์เน็ตได้
                            ซึ่งการเชื่อมต่อเหล่านั้นเองก็เลยมาเป็นแนวคิดที่ว่าอุปกรณ์เหล่านั้นก็ย่อมสามารถสื่อสารกันได้ด้วยเช่นกัน
                            โดยอาศัยตัว Sensor ในการสื่อสารถึงกัน นั่นแปลว่านอกจาก Smart Device
                            ต่างๆจะเชื่อมต่ออินเตอร์เน็ตได้แล้ว ยังสามารถเชื่อมต่อไปยังอุปกรณ์ตัวอื่นได้ด้วย<br>
                            https://www.aware.co.th/iot/
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-sm-3"></div>


        </div>


        <?php include('includes/footer.php') ?>
    </div>

    </div>

</body>

</html>